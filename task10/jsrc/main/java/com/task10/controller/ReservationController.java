package com.task10.controller;

import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.syndicate.deployment.annotations.environment.EnvironmentVariable;
import com.task10.model.Reservation;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import software.amazon.awssdk.services.dynamodb.DynamoDbClient;
import software.amazon.awssdk.services.dynamodb.model.AttributeValue;
import software.amazon.awssdk.services.dynamodb.model.PutItemRequest;
import software.amazon.awssdk.services.dynamodb.model.ResourceNotFoundException;
import software.amazon.awssdk.services.dynamodb.model.ScanRequest;
import software.amazon.awssdk.services.dynamodb.model.ScanResponse;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
@EnvironmentVariable(key = "reservations_table", value = "${reservations_table}")
public class ReservationController {

	private static final DynamoDbClient dynamoDbClient = DynamoDbClient.create();
	private static final ObjectMapper objectMapper = new ObjectMapper();

	private static final String RESERVATIONS_TABLE = System.getenv("reservations_table");

	public static APIGatewayProxyResponseEvent saveReservationsTable(APIGatewayProxyRequestEvent request) {
		try {
			Reservation reservation = objectMapper.readValue(request.getBody(), Reservation.class);
			String reservationId = UUID.randomUUID().toString();

			// Save the reservation in DynamoDB
			saveReservation(reservation, reservationId);

			Map<String, String> responseBody = new HashMap<>();
			responseBody.put("reservationId", reservationId);
			String responseBodyString = objectMapper.writeValueAsString(responseBody);

			// Return a successful response
			return new APIGatewayProxyResponseEvent()
				.withStatusCode(200)
				.withBody(responseBodyString);
		} catch (Exception e) {
			e.printStackTrace();
			return new APIGatewayProxyResponseEvent()
				.withStatusCode(400);
		}
	}

	public static APIGatewayProxyResponseEvent getReservationsTable() {
		try {
			List<Reservation> reservations = retrieveReservationsFromDynamoDB();

			Map<String, List<Reservation>> responseBody = new HashMap<>();
			responseBody.put("reservations", reservations);

			return new APIGatewayProxyResponseEvent()
				.withStatusCode(200)
				.withBody(objectMapper.writeValueAsString(responseBody));
		} catch (Exception e) {
			e.printStackTrace();
			// Return an error response
			return new APIGatewayProxyResponseEvent()
				.withStatusCode(400);
		}
	}

	private static void saveReservation(Reservation reservation, String reservationId) throws Exception {
		Map<String, AttributeValue> itemValues = new HashMap<>();
		itemValues.put("reservationId", AttributeValue.builder().s(reservationId).build());
		itemValues.put("tableNumber", AttributeValue.builder().n(String.valueOf(reservation.getTableNumber())).build());
		itemValues.put("clientName", AttributeValue.builder().s(reservation.getClientName()).build());
		itemValues.put("date", AttributeValue.builder().s(reservation.getPhoneNumber()).build());
		itemValues.put("phoneNumber", AttributeValue.builder().s(reservation.getDate()).build());
		itemValues.put("slotTimeStart", AttributeValue.builder().s(reservation.getSlotTimeStart()).build());
		itemValues.put("slotTimeEnd", AttributeValue.builder().s(reservation.getSlotTimeEnd()).build());

		PutItemRequest putItemRequest = PutItemRequest.builder()
			.tableName(RESERVATIONS_TABLE)
			.item(itemValues)
			.build();

		try {
			dynamoDbClient.putItem(putItemRequest);
		} catch (ResourceNotFoundException e) {
			throw new Exception("The specified DynamoDB table does not exist.", e);
		}
	}

	private static List<Reservation> retrieveReservationsFromDynamoDB() {
		ScanRequest scanRequest = ScanRequest.builder()
			.tableName("RESERVATIONS_TABLE")
			.build();

		// Perform the scan operation and retrieve the items
		ScanResponse scanResponse = dynamoDbClient.scan(scanRequest);
		List<Map<String, AttributeValue>> items = scanResponse.items();

		// Create a list to store the Reservation objects
		List<Reservation> reservations = new ArrayList<>();

		// Iterate over the items and convert them to Reservation objects
		for (Map<String, AttributeValue> item : items) {
			Reservation reservation = new Reservation();

			// Extract the data from the item and set it in the Reservation object
			reservation.setTableNumber(Integer.parseInt(item.get("tableNumber").n()));
			reservation.setClientName(item.get("clientName").s());
			reservation.setPhoneNumber(item.get("phoneNumber").s());
			reservation.setDate(item.get("date").s());
			reservation.setSlotTimeStart(item.get("slotTimeStart").s());
			reservation.setSlotTimeEnd(item.get("slotTimeEnd").s());

			// Add the Reservation object to the list
			reservations.add(reservation);
		}

		return reservations;
	}

}
