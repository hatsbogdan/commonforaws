package com.task10.controller;

import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.syndicate.deployment.annotations.environment.EnvironmentVariable;
import java.util.HashMap;
import java.util.Map;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import software.amazon.awssdk.services.dynamodb.DynamoDbClient;
import software.amazon.awssdk.services.dynamodb.model.AttributeValue;
import software.amazon.awssdk.services.dynamodb.model.GetItemRequest;
import software.amazon.awssdk.services.dynamodb.model.GetItemResponse;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
@EnvironmentVariable(key = "tables_table", value = "${tables_table}")
public class TableController {

	private static DynamoDbClient dynamoDbClient;

	private static final String DYNAMODB_TABLES_TABLE = System.getenv("tables_table");

	public static APIGatewayProxyResponseEvent getTableById(Integer tableId) {
		Map<String, AttributeValue> key = new HashMap<>();
		key.put("id", AttributeValue.builder().n(String.valueOf(tableId)).build());

		// Create the DynamoDB GetItemRequest
		GetItemRequest getItemRequest = GetItemRequest.builder()
			.tableName(DYNAMODB_TABLES_TABLE)
			.key(key)
			.build();

		// Retrieve the item from DynamoDB
		GetItemResponse getItemResponse = dynamoDbClient.getItem(getItemRequest);

		// Extract the attributes from the getItemResult
		Map<String, AttributeValue> item = getItemResponse.item();
		if (item == null || item.isEmpty()) {
			// Return a 404 Not Found response if the item is not found in DynamoDB
			return new APIGatewayProxyResponseEvent().withStatusCode(404);
		}

		// Extract the attribute values from the item
		Integer number = Integer.parseInt(item.get("number").n());
		Integer places = Integer.parseInt(item.get("places").n());
		Boolean isVip = Boolean.parseBoolean(String.valueOf(item.get("isVip").bool()));
		Integer minOrder = item.containsKey("minOrder") ? Integer.parseInt(item.get("minOrder").n()) : null;

		// Create the response object with the table data
		APIGatewayProxyResponseEvent response = new APIGatewayProxyResponseEvent();
		response.setStatusCode(200);
		response.setBody("{\"id\": " + tableId + ", \"number\": " + number + ", \"places\": " +
			places + ", \"isVip\": " + isVip + ", \"minOrder\": " + minOrder + "}");

		return response;
	}
}
