package com.task10.model;

import java.util.Optional;
import lombok.Data;

@Data
public class Table {

	private Integer id;
	private Integer number;
	private Integer places;
	private Boolean isVip;
	private Optional<Integer> minOrder;
}
