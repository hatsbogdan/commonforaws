package com.task10.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Reservation {

	private Integer tableNumber;
	private String clientName;
	private String phoneNumber;
	@JsonProperty("date")
	@JsonFormat(pattern = "yyyy-MM-dd")
	private String date;
	@JsonProperty("slotTimeStart")
	@JsonFormat(pattern = "HH:mm")
	private String slotTimeStart;
	@JsonProperty("slotTimeEnd")
	@JsonFormat(pattern = "HH:mm")
	private String slotTimeEnd;
}
