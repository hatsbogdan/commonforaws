package com.task10;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.syndicate.deployment.annotations.LambdaUrlConfig;
import com.syndicate.deployment.annotations.environment.EnvironmentVariable;
import com.syndicate.deployment.annotations.environment.EnvironmentVariables;
import com.syndicate.deployment.annotations.lambda.LambdaHandler;
import com.syndicate.deployment.model.lambda.url.AuthType;
import com.syndicate.deployment.model.lambda.url.InvokeMode;
import com.task10.model.Reservation;
import com.task10.utils.CognitoConfigurationRertieverUtils;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import software.amazon.awssdk.services.cognitoidentityprovider.CognitoIdentityProviderClient;
import software.amazon.awssdk.services.cognitoidentityprovider.model.AdminCreateUserRequest;
import software.amazon.awssdk.services.cognitoidentityprovider.model.AdminCreateUserResponse;
import software.amazon.awssdk.services.cognitoidentityprovider.model.AdminInitiateAuthRequest;
import software.amazon.awssdk.services.cognitoidentityprovider.model.AdminInitiateAuthResponse;
import software.amazon.awssdk.services.cognitoidentityprovider.model.AdminSetUserPasswordRequest;
import software.amazon.awssdk.services.cognitoidentityprovider.model.AttributeType;
import software.amazon.awssdk.services.cognitoidentityprovider.model.AuthFlowType;
import software.amazon.awssdk.services.cognitoidentityprovider.model.NotAuthorizedException;
import software.amazon.awssdk.services.dynamodb.DynamoDbClient;
import software.amazon.awssdk.services.dynamodb.model.AttributeValue;
import software.amazon.awssdk.services.dynamodb.model.DynamoDbException;
import software.amazon.awssdk.services.dynamodb.model.GetItemRequest;
import software.amazon.awssdk.services.dynamodb.model.GetItemResponse;
import software.amazon.awssdk.services.dynamodb.model.PutItemRequest;
import software.amazon.awssdk.services.dynamodb.model.ResourceNotFoundException;
import software.amazon.awssdk.services.dynamodb.model.ScanRequest;
import software.amazon.awssdk.services.dynamodb.model.ScanResponse;

@LambdaHandler(lambdaName = "api_handler",
	roleName = "api_handler-role"
)
@LambdaUrlConfig(
	authType = AuthType.NONE,
	invokeMode = InvokeMode.BUFFERED
)
@EnvironmentVariables(value = {
	@EnvironmentVariable(key = "tables_table", value = "${tables_table}"),
	@EnvironmentVariable(key = "reservations_table", value = "${reservations_table}")
})
public class ApiHandler implements RequestHandler<APIGatewayProxyRequestEvent, APIGatewayProxyResponseEvent> {

	private String DYNAMODB_TABLES_TABLE = System.getenv("tables_table");
	private String DYNAMODB_RESERVATIONS_TABLE = System.getenv("reservations_table");
	private static final int SC_OK = 200;
	private static final int SC_BAD_REQUEST = 400;
	private static final int SC_UNAUTHORIZED = 401;
	private final DynamoDbClient dynamoDbClient;
	private final CognitoIdentityProviderClient cognitoClient;
	private final ObjectMapper objectMapper;

	public ApiHandler() {
		dynamoDbClient = DynamoDbClient.create();
		cognitoClient = CognitoIdentityProviderClient.create();
		objectMapper = new ObjectMapper();
	}

	@Override
	public APIGatewayProxyResponseEvent handleRequest(APIGatewayProxyRequestEvent request, Context context) {
		System.out.println("APIGatewayProxyRequestEvent request: " + request);
		String resource = request.getResource();
		System.out.println("Resource: " + resource);
		System.out.println("HttpMethod: " + request.getHttpMethod());

		String userPoolId = CognitoConfigurationRertieverUtils.getUserPoolId(cognitoClient);
		String clientId = CognitoConfigurationRertieverUtils.getClientId(cognitoClient, userPoolId);

		String requestBody = request.getBody();
		try {
			JsonNode jsonNode;

			if ("/signin".equals(resource) && "POST".equals(request.getHttpMethod())) {
				jsonNode = objectMapper.readTree(requestBody);
				String email = getEmailFromRequestBody(jsonNode);
				String password = getPasswordFromRequestBody(jsonNode);
				return signIn(email, password, userPoolId, clientId);
			} else if ("/signup".equals(resource) && "POST".equals(request.getHttpMethod())) {
				jsonNode = objectMapper.readTree(requestBody);
				String firstName = jsonNode.get("firstName").textValue();
				String lastName = jsonNode.get("lastName").textValue();
				String email = getEmailFromRequestBody(jsonNode);
				String password = getPasswordFromRequestBody(jsonNode);
				return signUp(email, password, firstName, lastName, userPoolId);
			} else if ("/tables".equals(resource) && "GET".equals(request.getHttpMethod())) {
				return getTables();
			} else if ("/tables/{tableId}".equals(resource) && "GET".equals(request.getHttpMethod())) {
				int tableId = extractPositiveNumber(request.getPath());
				System.out.println("tableId: " + tableId);
				return getTableById(tableId);
			} else if ("/tables".equals(resource) && "POST".equals(request.getHttpMethod())) {
				return saveTablesTable(request);
			} else if ("/reservations".equals(resource) && "GET".equals(request.getHttpMethod())) {
				return getReservationsTable();
			} else if ("/reservations".equals(resource) && "POST".equals(request.getHttpMethod())) {
				return saveReservationsTable(request);
			} else {
				return new APIGatewayProxyResponseEvent()
					.withStatusCode(SC_BAD_REQUEST)
					.withBody("Invalid resource or HTTP method");
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	private APIGatewayProxyResponseEvent signIn(String email, String password, String userPoolId, String clientId) {
		Map<String, String> map = new HashMap<>();
		map.put("USERNAME", email);
		map.put("PASSWORD", password);
		try {
			// Create the sign-in request using the email and password
			AdminInitiateAuthRequest signInRequest = AdminInitiateAuthRequest.builder()
				.userPoolId(userPoolId)
				.clientId(clientId)
				.authFlow(AuthFlowType.ADMIN_USER_PASSWORD_AUTH)
				.authParameters(map)
				.build();
			System.out.println("signInRequest: " + signInRequest.toString());

			// Call the Cognito Identity Provider API to sign in the user
			AdminInitiateAuthResponse signInResponse =
				cognitoClient.adminInitiateAuth(signInRequest);
			System.out.println("signInResponse: " + signInResponse.toString());

			if (signInResponse.authenticationResult() != null) {
				// Retrieve the access token from the sign-in response
				String accessToken = signInResponse.authenticationResult().idToken();
				System.out.println("accessToken: " + accessToken);

				// Return the access token in the response
				return new APIGatewayProxyResponseEvent()
					.withStatusCode(SC_OK)
					.withBody("{\"accessToken\": \"" + accessToken + "\"}");
			} else {
				return new APIGatewayProxyResponseEvent()
					.withStatusCode(SC_UNAUTHORIZED)
					.withBody("Authentication failed: Invalid response from authentication service");
			}
		} catch (NotAuthorizedException e) {
			return new APIGatewayProxyResponseEvent()
				.withStatusCode(SC_UNAUTHORIZED)
				.withBody("Authentication failed: " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			return new APIGatewayProxyResponseEvent()
				.withStatusCode(SC_BAD_REQUEST)
				.withBody("Failed to sign in: " + e.getMessage());
		}
	}

	private APIGatewayProxyResponseEvent signUp(String email, String password, String firstname,
												String lastname, String userPoolId) {
		AttributeType userAttrs = AttributeType.builder()
			.name("email")
			.value(email)
			.build();
		try {

			// Create the user in the Cognito User Pool using the AdminCreateUser API
			AdminCreateUserRequest createUserRequest = AdminCreateUserRequest.builder()
				.userAttributes(userAttrs)
				.userPoolId(userPoolId)
				.username(email)
				.temporaryPassword(password)  // Assuming temporary password is allowed
				.messageAction("SUPPRESS")  // To avoid email sending
				.build();

			return getApiGatewayProxyResponseEventCreateUser(createUserRequest, userPoolId, password);
		} catch (Exception e) {
			return new APIGatewayProxyResponseEvent()
				.withStatusCode(SC_BAD_REQUEST)
				.withBody("Invalid request body");
		}
	}

	private APIGatewayProxyResponseEvent getApiGatewayProxyResponseEventCreateUser(
		AdminCreateUserRequest createUserRequest, String userPoolId, String password) {
		try {
			AdminCreateUserResponse createUserResponse = cognitoClient.adminCreateUser(createUserRequest);

			// Get the user's username from the createUserResponse
			String username = createUserResponse.user().username();

			// Call AdminSetUserPassword to update the user's password to a permanent one
			AdminSetUserPasswordRequest setUserPasswordRequest = AdminSetUserPasswordRequest.builder()
				.userPoolId(userPoolId)
				.username(username)
				.password(password) // The permanent password
				.permanent(true)
				.build();

			cognitoClient.adminSetUserPassword(setUserPasswordRequest);
			System.out.println("User created: " + createUserResponse.user());
			return new APIGatewayProxyResponseEvent()
				.withStatusCode(SC_OK)
				.withBody("User created successfully");
		} catch (Exception exception) {
			return new APIGatewayProxyResponseEvent()
				.withStatusCode(SC_BAD_REQUEST)
				.withBody("Failed to create user: " + exception.getMessage());
		}
	}

	private APIGatewayProxyResponseEvent saveTablesTable(APIGatewayProxyRequestEvent request) {
		Map<String, Object> requestBody = new HashMap<>();
		try {
			requestBody = objectMapper.readValue(request.getBody(),
				new TypeReference<Map<String, Object>>() {});
		} catch (IOException e) {
			e.printStackTrace();
		}

		Integer minOrder = (Integer) requestBody.getOrDefault("minOrder", null);
		Integer id = (Integer) requestBody.get("id");
		Integer number = (Integer) requestBody.get("number");
		Integer places = (Integer) requestBody.get("places");
		Boolean isVip = (Boolean) requestBody.get("isVip");

		Map<String, AttributeValue> itemValues = new HashMap<>();
		itemValues.put("id", AttributeValue.builder().n(id.toString()).build());
		itemValues.put("number", AttributeValue.builder().n(number.toString()).build());
		itemValues.put("places", AttributeValue.builder().n(places.toString()).build());
		itemValues.put("isVip", AttributeValue.builder().bool(isVip).build());

		if (minOrder != null) {
			itemValues.put("minOrder", AttributeValue.builder().n(minOrder.toString()).build());
		}

		PutItemRequest putItemRequest = PutItemRequest.builder()
			.tableName(DYNAMODB_TABLES_TABLE)
			.item(itemValues)
			.build();

		try {
			dynamoDbClient.putItem(putItemRequest);
		} catch (DynamoDbException e) {
			e.printStackTrace();
		}

		Map<String, Object> responseBody = new HashMap<>();
		responseBody.put("id", id);
		String responseBodyString = null;
		try {
			responseBodyString = objectMapper.writeValueAsString(responseBody);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}

		return new APIGatewayProxyResponseEvent()
			.withStatusCode(200)
			.withBody(responseBodyString);
	}

	private APIGatewayProxyResponseEvent getTables() {
		ScanRequest scanRequest = ScanRequest.builder()
			.tableName(DYNAMODB_TABLES_TABLE)
			.build();

		ScanResponse scanResponse = dynamoDbClient.scan(scanRequest);

		List<Map<String, AttributeValue>> items = scanResponse.items();

		List<Map<String, Object>> tables = new ArrayList<>();
		for (Map<String, AttributeValue> item : items) {
			Map<String, Object> table = new HashMap<>();
			table.put("id", Integer.parseInt(item.get("id").n()));
			table.put("number", Integer.parseInt(item.get("number").n()));
			table.put("places", Integer.parseInt(item.get("places").n()));
			table.put("isVip", Boolean.parseBoolean(item.get("isVip").bool().toString()));

			if (item.containsKey("minOrder")) {
				table.put("minOrder", Integer.parseInt(item.get("minOrder").n()));
			}

			tables.add(table);
		}

		Map<String, Object> responseBody = new HashMap<>();
		responseBody.put("tables", tables);

		String responseBodyString = null;
		try {
			responseBodyString = objectMapper.writeValueAsString(responseBody);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}

		return new APIGatewayProxyResponseEvent()
			.withStatusCode(SC_OK)
			.withBody(responseBodyString);
	}

	private APIGatewayProxyResponseEvent saveReservationsTable(APIGatewayProxyRequestEvent request) {
		try {
			Reservation reservation = objectMapper.readValue(request.getBody(), Reservation.class);
			System.out.println("reservation saveReservationsTable: " + reservation);
			String reservationId = UUID.randomUUID().toString();
			int tableNumber = reservation.getTableNumber();
			System.out.println("tableNumber: " + tableNumber);

			if (!isTableNumberExists(tableNumber)) {
				return new APIGatewayProxyResponseEvent()
					.withStatusCode(400)
					.withBody("The specified table number does not exist.");
			}

			// Check for reservation conflicts
			if (hasReservationConflict(reservation)) {
				return new APIGatewayProxyResponseEvent()
					.withStatusCode(400)
					.withBody("There is a conflict with an existing reservation.");
			}

			// Save the reservation in DynamoDB
			saveReservation(reservation);

			Map<String, String> responseBody = new HashMap<>();
			responseBody.put("reservationId", reservationId);
			String responseBodyString = objectMapper.writeValueAsString(responseBody);

			// Return a successful response
			return new APIGatewayProxyResponseEvent()
				.withStatusCode(200)
				.withBody(responseBodyString);
		} catch (Exception e) {
			e.printStackTrace();
			return new APIGatewayProxyResponseEvent()
				.withStatusCode(400);
		}
	}

	private APIGatewayProxyResponseEvent getReservationsTable() {
		try {
			ScanRequest scanRequest = ScanRequest.builder()
				.tableName(DYNAMODB_RESERVATIONS_TABLE)
				.build();

			ScanResponse scanResponse = dynamoDbClient.scan(scanRequest);

			List<Map<String, AttributeValue>> items = scanResponse.items();

			List<Map<String, Object>> tables = new ArrayList<>();
			for (Map<String, AttributeValue> item : items) {
				Map<String, Object> table = new HashMap<>();
				table.put("tableNumber", Integer.parseInt(item.get("id").n()));
				table.put("clientName", item.get("clientName").s());
				table.put("phoneNumber", item.get("date").s());
				table.put("date", item.get("phoneNumber").s());
				table.put("slotTimeStart", item.get("slotTimeStart").s());
				table.put("slotTimeEnd", item.get("slotTimeEnd").s());

				tables.add(table);
			}

			Map<String, Object> responseBody = new HashMap<>();
			responseBody.put("reservations", tables);

			String responseBodyString = null;
			try {
				responseBodyString = objectMapper.writeValueAsString(responseBody);
			} catch (JsonProcessingException e) {
				e.printStackTrace();
			}

			System.out.println("responseBodyString: " + responseBodyString);
			return new APIGatewayProxyResponseEvent()
				.withStatusCode(SC_OK)
				.withBody(responseBodyString);
		} catch (Exception e) {
			e.printStackTrace();
			// Return an error response
			return new APIGatewayProxyResponseEvent()
				.withStatusCode(400);
		}
	}

	private void saveReservation(Reservation reservation) throws Exception {
		Map<String, AttributeValue> itemValues = new HashMap<>();
		itemValues.put("id", AttributeValue.builder().n(String.valueOf(reservation.getTableNumber())).build());
		itemValues.put("clientName", AttributeValue.builder().s(reservation.getClientName()).build());
		itemValues.put("date", AttributeValue.builder().s(reservation.getPhoneNumber()).build());
		itemValues.put("phoneNumber", AttributeValue.builder().s(reservation.getDate()).build());
		itemValues.put("slotTimeStart", AttributeValue.builder().s(reservation.getSlotTimeStart()).build());
		itemValues.put("slotTimeEnd", AttributeValue.builder().s(reservation.getSlotTimeEnd()).build());

		PutItemRequest putItemRequest = PutItemRequest.builder()
			.tableName(DYNAMODB_RESERVATIONS_TABLE)
			.item(itemValues)
			.build();

		try {
			dynamoDbClient.putItem(putItemRequest);
		} catch (ResourceNotFoundException e) {
			throw new Exception("The specified DynamoDB table does not exist.", e);
		}
	}

	private List<Reservation> retrieveReservationsFromDynamoDB() {
		ScanRequest scanRequest = ScanRequest.builder()
			.tableName(DYNAMODB_RESERVATIONS_TABLE)
			.build();

		// Perform the scan operation and retrieve the items
		ScanResponse scanResponse = dynamoDbClient.scan(scanRequest);
		List<Map<String, AttributeValue>> items = scanResponse.items();

		// Create a list to store the Reservation objects
		List<Reservation> reservations = new ArrayList<>();

		// Iterate over the items and convert them to Reservation objects
		for (Map<String, AttributeValue> item : items) {
			Reservation reservation = new Reservation();

			// Extract the data from the item and set it in the Reservation object
			reservation.setTableNumber(Integer.parseInt(item.get("id").n()));
			reservation.setClientName(item.get("clientName").s());
			reservation.setPhoneNumber(item.get("phoneNumber").s());
			reservation.setDate(item.get("date").s());
			reservation.setSlotTimeStart(item.get("slotTimeStart").s());
			reservation.setSlotTimeEnd(item.get("slotTimeEnd").s());

			// Add the Reservation object to the list
			reservations.add(reservation);
		}

		reservations.forEach(reservation -> System.out.println(reservation.toString()));
		return reservations;
	}

	private APIGatewayProxyResponseEvent getTableById(Integer tableId) {
		Map<String, AttributeValue> key = new HashMap<>();
		key.put("id", AttributeValue.builder().n(String.valueOf(tableId)).build());

		// Create the DynamoDB GetItemRequest
		GetItemRequest getItemRequest = GetItemRequest.builder()
			.tableName(DYNAMODB_TABLES_TABLE)
			.key(key)
			.build();

		// Retrieve the item from DynamoDB
		GetItemResponse getItemResponse = dynamoDbClient.getItem(getItemRequest);

		// Extract the attributes from the getItemResult
		Map<String, AttributeValue> item = getItemResponse.item();
		if (item == null || item.isEmpty()) {
			// Return a 404 Not Found response if the item is not found in DynamoDB
			return new APIGatewayProxyResponseEvent().withStatusCode(404);
		}

		// Extract the attribute values from the item
		int number = Integer.parseInt(item.get("number").n());
		int places = Integer.parseInt(item.get("places").n());
		boolean isVip = Boolean.parseBoolean(String.valueOf(item.get("isVip").bool()));
		Integer minOrder = item.containsKey("minOrder") ? Integer.parseInt(item.get("minOrder").n()) : null;

		// Create the response object with the table data
		APIGatewayProxyResponseEvent response = new APIGatewayProxyResponseEvent();
		response.setStatusCode(200);
		response.setBody("{\"id\": " + tableId + ", \"number\": " + number + ", \"places\": " +
			places + ", \"isVip\": " + isVip + ", \"minOrder\": " + minOrder + "}");

		return response;
	}

	private String getEmailFromRequestBody(JsonNode jsonNode) {
		return jsonNode.get("email").textValue();
	}

	private String getPasswordFromRequestBody(JsonNode jsonNode) {
		return jsonNode.get("password").textValue();
	}

	private boolean hasOverlappingReservations(Reservation newReservation) {
		// Retrieve existing reservations from DynamoDB

		List<Reservation> existingReservations = retrieveReservationsFromDynamoDB();

		// Check for overlap with each existing reservation
		for (Reservation existingReservation : existingReservations) {
			if (doesOverlap(existingReservation, newReservation)) {
				return true;
			}
		}

		return false;
	}

	private boolean doesOverlap(Reservation reservation1, Reservation reservation2) {
		// Compare the slot times and dates to check for overlap
		String date1 = reservation1.getDate();
		String date2 = reservation2.getDate();
		String slotTimeStart1 = reservation1.getSlotTimeStart();
		String slotTimeEnd1 = reservation1.getSlotTimeEnd();
		String slotTimeStart2 = reservation2.getSlotTimeStart();
		String slotTimeEnd2 = reservation2.getSlotTimeEnd();

		// Compare the dates
		if (!date1.equals(date2)) {
			return false; // No overlap in dates
		}

		// Compare the slot times
		return slotTimeStart1.compareTo(slotTimeEnd2) < 0 && slotTimeStart2.compareTo(slotTimeEnd1) < 0;
	}

	private static int extractPositiveNumber(String input) {
		// Create a pattern to match positive integer values
		Pattern pattern = Pattern.compile("\\d+");
		Matcher matcher = pattern.matcher(input);

		// Find the first occurrence of a positive integer value
		if (matcher.find()) {
			String number = matcher.group();
			return Integer.parseInt(number);
		} else {
			// If no positive integer found, you can return a default value or throw an exception
			throw new IllegalArgumentException("No positive integer found in the input string.");
		}
	}

	private boolean isTableNumberExists(int tableNumber) {
		Map<String, AttributeValue> expressionAttributeValues = new HashMap<>();
		expressionAttributeValues.put(":val", AttributeValue.builder().n(String.valueOf(tableNumber)).build());

		Map<String, String> expressionAttributeNames = new HashMap<>();
		expressionAttributeNames.put("#num", "number");

		ScanResponse scanResponse = dynamoDbClient.scan(ScanRequest.builder()
			.tableName(DYNAMODB_TABLES_TABLE)
			.filterExpression("#num = :val")
			.expressionAttributeValues(expressionAttributeValues)
			.expressionAttributeNames(expressionAttributeNames)
			.projectionExpression("#num")
			.build());

		return !scanResponse.items().isEmpty();
	}

	private boolean hasReservationConflict(Reservation newReservation) {
		ScanRequest scanRequest = ScanRequest.builder()
			.tableName(DYNAMODB_RESERVATIONS_TABLE)
			.build();

		ScanResponse scanResponse = dynamoDbClient.scan(scanRequest);

		List<Map<String, AttributeValue>> items = scanResponse.items();

		for (Map<String, AttributeValue> item : items) {
			// Convert the DynamoDB item to a Reservation object
			Reservation existingReservation = convertToReservation(item);


			// Check for conflict conditions
			if (isConflict(existingReservation, newReservation)) {
				return true; // Conflict found
			}
		}

		return false; // No conflicts found
	}

	private Reservation convertToReservation(Map<String, AttributeValue> item) {
		Reservation reservation = new Reservation();
		// Assuming your Reservation model has appropriate setters for the attributes

		reservation.setTableNumber(Integer.parseInt(item.get("id").n()));
		reservation.setClientName(item.get("clientName").s());
		reservation.setPhoneNumber(item.get("phoneNumber").s());
		reservation.setDate(item.get("date").s());
		reservation.setSlotTimeStart(item.get("slotTimeStart").s());
		reservation.setSlotTimeEnd(item.get("slotTimeEnd").s());

		System.out.println("convertToReservation reservation: " + reservation);
		return reservation;
	}

	private boolean isConflict(Reservation existingReservation, Reservation newReservation) {
		// Check if the table numbers are the same
		System.out.println("isConflict existingReservation: " + existingReservation);
		System.out.println("isConflict newReservation: " + newReservation);
		System.out.println("isConflict Objects.equals(existingReservation.getTableNumber(): " +
			Objects.equals(existingReservation.getTableNumber(), newReservation.getTableNumber()));
		System.out.println("isConflict Objects.equals(existingReservation.getSlotTimeStart(): " +
			Objects.equals(existingReservation.getSlotTimeStart(), newReservation.getSlotTimeStart()));
		System.out.println("isConflict Objects.equals(existingReservation.getSlotTimeEnd(): " +
			Objects.equals(existingReservation.getSlotTimeEnd(), newReservation.getSlotTimeEnd()));
		return Objects.equals(existingReservation.getTableNumber(), newReservation.getTableNumber())
			&& Objects.equals(existingReservation.getSlotTimeStart(), newReservation.getSlotTimeStart())
			&& Objects.equals(existingReservation.getSlotTimeEnd(), newReservation.getSlotTimeEnd());
	}

}
