package com.task10.utils;

import java.util.List;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import software.amazon.awssdk.services.cognitoidentityprovider.CognitoIdentityProviderClient;
import software.amazon.awssdk.services.cognitoidentityprovider.model.ListUserPoolClientsRequest;
import software.amazon.awssdk.services.cognitoidentityprovider.model.ListUserPoolClientsResponse;
import software.amazon.awssdk.services.cognitoidentityprovider.model.ListUserPoolsRequest;
import software.amazon.awssdk.services.cognitoidentityprovider.model.ListUserPoolsResponse;
import software.amazon.awssdk.services.cognitoidentityprovider.model.UserPoolClientDescription;
import software.amazon.awssdk.services.cognitoidentityprovider.model.UserPoolDescriptionType;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class CognitoConfigurationRertieverUtils {

	public static String getUserPoolId(CognitoIdentityProviderClient cognitoClient) {
		ListUserPoolsRequest request = ListUserPoolsRequest.builder()
			.maxResults(10)
			.build();

		ListUserPoolsResponse response = cognitoClient.listUserPools(request);
		List<UserPoolDescriptionType> userPools = response.userPools();

		if (!userPools.isEmpty()) {
			// Retrieve the first user pool's ID
			UserPoolDescriptionType userPool = userPools.get(0);
			return userPool.id();
		}

		throw new RuntimeException("No user pools found");
	}

	public static String getClientId(CognitoIdentityProviderClient cognitoClient, String userPoolId) {
		ListUserPoolClientsRequest request = ListUserPoolClientsRequest.builder()
			.userPoolId(userPoolId)
			.maxResults(1)
			.build();

		ListUserPoolClientsResponse response = cognitoClient.listUserPoolClients(request);
		List<UserPoolClientDescription> userPoolClients = response.userPoolClients();

		if (!userPoolClients.isEmpty()) {
			UserPoolClientDescription userPoolClient = userPoolClients.get(0);
			return userPoolClient.clientId();
		}

		throw new RuntimeException("No user pool clients found");
	}
}
